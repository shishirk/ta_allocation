
The script used the list MTech/PhD students to allocate them to courses
as requested by the instructors. The requests are collected from a shared
google spreadsheet (see "make gen-template" below) which contains the list
of courses and the number of TAs requested for the courses.

Instructors can specify to allocate specific TAs to their courses in the
sheet named "requested_TAs". 

Following rules are observed during allocation:

- Project funded PG students are not considered only if requested by the associated faculty member. This is to provide them some exposure to teaching 
- PhD students in 4th year or lower are considered
- External PhD candidates or ACMTech students are not considered
- First year PhD students will be given less load (so they can devote more time for learning)
- PG students are allocated to the same stream, if possible, else CSP and Micro can be mixed
- For PG courses, PhD students in 3/4 years are allocated first
- For PG courses, the students who are specifically named by the instructor are allocated to respective courses
- For UG courses govt. funded Mtechs are allocated first
- Some PhD students are also allocated to large UG courses, to provide them exposure.

The script converts the xlsx file to a SQL database (in memory) to do the required
selection and allocations. The updated database is finally written to a .xlsx
file ("gen-alloc.xlsx").


Here are some more comments from the makefile on preparing the data:

Generate the template using make gen-template, it will give templ.xlsx
Fill this file with Timetable data, enrolled PG students and eligible UG students
Ask colleagues to fill in the Requested_TAs sheet for reserving particular students
The same data has to be manually fed into allocation sheet at present [todo]
The year number should be tweaked in queries if the base year changes [todo]
Any special requests can be implemented manually before feeding it to the script.
ACM Mtech or External PhD candidates can be directly removed from the sheet.
The req_ta column should be filled for all courses.
The list of PhD/Mtech students should specify their streams, example lists for 2020 are provided.
Run make alloc for allocation using ta-alloc.xlsx

