
.PHONY: help
help: ## Display this help section
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'
.DEFAULT_GOAL := help

.PHONY: gen-template
gen-template: ## generate template for filling the data in
	python3 ta_alloc.py template

.PHONY: alloc
alloc: ## using file ta-alloc.xlsx (format is of template, use make gen-template), output is kept in gen_alloc.xlsx.
	python3 ta_alloc.py -f ta-alloc.xlsx

.PHONY: data
data: ## how / which data to collect.
	@echo "Generate the template using "make gen-template", it will give templ.xlsx"
	@echo "Fill this file with Timetable data, enrolled PG students and eligible UG students"
	@echo "Ask colleagues to fill in the Requested_TAs sheet for reserving particular students"
	@echo "The same data has to be manually fed into allocation sheet at present [todo]"
	@echo "The year number should be tweaked in queries if the base year changes [todo]"
	@echo "Any special requests can be implemented manually before feeding it to the script."
	@echo "ACM Mtech or External PhD candidates can be directly removed from the sheet."
	@echo "The req_ta column should be filled for all courses."
	@echo "The list of PhD/Mtech students should specify their streams, example lists for 2020 are provided."
	@echo "Run make alloc for allocation using ta-alloc.xlsx"

