import sys
import pandas as pd
from pandas import ExcelWriter
import sqlite3

# globals and some constant strings.
db = sqlite3.connect(':memory:')
e = db.execute
seg_dict = {12: 1, 34: 2, 56: 3}
segment_order = 'ORDER BY CASE segments WHEN 16 THEN 0 WHEN 14 THEN 1 \
    WHEN 36 THEN 2 WHEN 12 THEN 3 WHEN 34 THEN 4 WHEN 56 THEN 5 END'
stream_order = 'ORDER BY CASE stream WHEN \'ML\' THEN 0 WHEN \'S&C\' THEN 1 \
        WHEN \'CSP\' THEN 2 WHEN \'Micro\' THEN 3 WHEN \'CSP\' THEN 4 END'
roll_order = ' ORDER by rollno DESC'

def pprint(loc, q):
    #res = db.execute(q)
    #for r in res: print(r)
    print(loc, ' >>\n', pd.read_sql_query(q, db))
def chop(segment):
    if segment == 16: return [12, 34, 56]
    if segment == 14: return [12, 34]
    if segment == 36: return [34, 56]
    return [segment]
def match_seg(seg, s):
    if seg==16: return s[0]==None and s[1]==None and s[2]==None
    if seg==14: return s[0]==None and s[1]==None 
    if seg==36: return s[1]==None and s[2]==None
    if seg==12: return s[0]==None
    if seg==34: return s[1]==None 
    if seg==56: return s[2]==None
    return False

def fill_slots(Sq, USq, Cq, UCq, no_stream=True):
    C = e(Cq)
    for c in C:
        code, req, segs, alloc_ta, num_ta, cstream = c
        print(c)
        if num_ta is None: num_ta, alloc_ta = 0, ''
        S, i = e(Sq).fetchall(), 0
        while num_ta < req and i<len(S):
            s = list(S[i])
            if s[4] is None: s[4] = 0
            stud_seg = [s[1], s[2], s[3]]
            #print(c, s)
            if (no_stream or (cstream==s[5])) and match_seg(segs, stud_seg) is True:
                #print('assigned', code)
                alloc_ta += s[0].strip()+' '
                num_ta += 1
                #print(alloc_ta, num_ta)
                for z in chop(segs):
                    s[seg_dict[z]] = code
                    s[4] += 1
                e(USq, (s[1], s[2], s[3], s[4], s[0]))
                db.commit()
            i+=1
        #print("commit", alloc_ta, num_ta, code) 
        e(UCq, (alloc_ta, num_ta, code))
        db.commit()

def gen_template():
    writer = ExcelWriter('templ.xlsx')
    allocation_dict = {'Instructor': ['Dr.Amit'], 'ccode':['EE6410'], 'cname': ['Biomedical IC Design'],
            'expected_students': [5], 'req_tas': [1], 'segments': [14], 'stream1': ['Micro'], 
            'stream2': ['CSP'], 'stream3': ['S&C'], 'alloc_ta_list': ['EE16RESCH11006 EE16RESCH11005'], 'num_ta':[2] }
    df_a = pd.DataFrame(allocation_dict)
    df_a.to_excel(writer, sheet_name='Allocation')
    pg_dict = {'entry_year': ['2015_Jan'], 'name': ['Purnachand Simhadri'], 'rollno':['EE15RESCH01001'],
            'stream': ['CSP'], 'funding':['MHRD'], 'segment1':['EE6141'], 'segment2': [''], 'segment3': [''],
            'load': [1], 'emailed?':['']}
    df_p = pd.DataFrame(pg_dict)
    df_p.to_excel(writer, sheet_name='PG_alloc')
    del(pg_dict['funding'])
    df_u = pd.DataFrame(pg_dict)
    df_u.to_excel(writer, sheet_name='UG_alloc')
    df_p.to_excel(writer, sheet_name='Requested_TAs')
    writer.save()

def write_alloc_sheet():
    writer = ExcelWriter('gen_alloc.xlsx')
    pd.read_sql_query('select * from allocation', db).to_excel(writer, 'Allocation', index=False)
    pd.read_sql_query('select * from pg_ta', db).to_excel(writer, 'PG_alloc', index=False)
    pd.read_sql_query('select * from ug_ta', db).to_excel(writer, 'UG_alloc', index=False)
    pd.read_sql_query('select * from req_allotment', db).to_excel(writer, 'Requested_TAs', index=False)
    writer.save()


def make_allotment(fname='templ.xlsx'):
    dfs = pd.read_excel(fname, sheet_name=None)
    pg = dfs['PG_alloc'].to_sql('pg_ta', db)
    ug = dfs['UG_alloc'].to_sql('ug_ta', db)
    allocation = dfs['Allocation'].to_sql('allocation', db)
    #print(allocation)
    req_allotment = dfs['Requested_TAs'].to_sql('req_allotment', db)

    UCq = 'UPDATE allocation SET alloc_ta_list=?, num_ta=? WHERE ccode=? COLLATE NOCASE'
    ## First of all allot req_allotment to the pg_alloc sheet
    R = e('select rollno, segment1, segment2, segment3, load from req_allotment')
    for r in R:
        e('update pg_ta set segment1=?, segment2=?, segment3=?, load=? where rollno=? COLLATE NOCASE', 
            (r[1], r[2], r[3], r[4], r[0]))
    db.commit()
    #pprint('merge', 'select rollno, name, segment1 from pg where rollno=\'ee18resch01003\' COLLATE NOCASE')


    ## Now allocate MTech20, MTech19 to UG courses starting from M19 to UG1st year,
    ## then M18 to UG2nd year and higher. Full load will be given to all MTechs
    ### MTech19 to UG1st yr
    S1q = 'SELECT rollno, segment1, segment2, segment3, load, stream FROM pg_ta WHERE (load IS NULL OR load<3) '
    S2q = 'AND (rollno LIKE \'ee20m%\' OR rollno LIKE \'ee19m%\')'
    Sq = S1q + S2q + roll_order

    USq = 'UPDATE pg_ta SET segment1=?, segment2=?, segment3=?, load=? WHERE rollno=? COLLATE NOCASE'
    
    C1q = 'SELECT ccode, req_tas, segments, alloc_ta_list, num_ta, stream1 FROM allocation WHERE '
    C2q = '(ccode LIKE \'EE1%\' or ccode LIKE \'ID1%\') AND '
    C3q = ' (num_ta IS NULL OR req_tas > num_ta) '
    Cq = C1q + C2q + C3q + segment_order

    fill_slots(Sq, USq, Cq, UCq)
    #Pq1 = 'SELECT rollno, segment1, segment2, segment3, load FROM pg_ta WHERE rollno LIKE \'ee20m%\' OR rollno LIKE \'ee19m%\''
    #Pq2 = 'SELECT ccode, req_tas, segments, alloc_ta_list, num_ta FROM allocation WHERE \
    #    (ccode LIKE \'EE1%\' or ccode LIKE \'ID1%\' or ccode LIKE \'AI1%\' or ccode LIKE \'EE2%\' or ccode LIKE \'EE3%\')'
    #pprint('test', Pq1)
    #pprint('test', Pq2)
    

    ### MTech19 + MTech18 to UG2nd yr onwards, match stream this time. Assign 19 batch first.
    C2q = ' (ccode LIKE \'AI1%\' or ccode LIKE \'EE2%\' or ccode LIKE \'EE3%\') AND '
    Cq = C1q + C2q + C3q #+ ' ORDER BY ccode'
    fill_slots(Sq, USq, Cq, UCq, no_stream=False)
    #pprint('test', Pq1)
    #pprint('test', Pq2)

    ## Now use MTech18, resch18, resch17 to fill PG5 level courses, match stream. resch19 are exempted.
    S2q = 'AND (rollno LIKE \'ee19m%\' OR rollno LIKE \'ee19r%\' or rollno LIKE \'ee18r%\')'
    S3q = ' AND (funding NOT LIKE \'ext%\') '
    Sq = S1q + S2q + S3q + roll_order
    C2q = ' (ccode LIKE \'EE2%\' or ccode LIKE \'EE3%\' or ccode LIKE \'EE5%\') AND '
    Cq = C1q + C2q + C3q + ' ORDER by ccode'
    fill_slots(Sq, USq, Cq, UCq, no_stream=False)
    #Pq1 = 'SELECT rollno, segment1, segment2, segment3, load FROM pg_ta WHERE rollno \
    #        LIKE \'ee19m%\' OR rollno LIKE \'ee19r%\' or rollno LIKE \'ee18r%\''
    #Pq2 = 'SELECT ccode, req_tas, segments, alloc_ta_list, num_ta FROM allocation WHERE \
    #    (ccode LIKE \'EE5%\' or ccode LIKE \'EE2%\' or ccode LIKE \'EE3%\')'
    #pprint('test', Pq1)
    #pprint('test', Pq2)


    ## Now use resch18, resch17, resch16 to fill PG6 level courses, match stream.
    S2q = 'AND (rollno LIKE \'ee19r%\' OR rollno LIKE \'ee18r%\' or rollno LIKE \'ee17r%\')'
    S3q = ' AND (funding NOT LIKE \'ext%\') '
    Sq = S1q + S2q + S3q + roll_order
    C2q = ' (ccode LIKE \'EE6%\') AND '
    Cq = C1q + C2q + C3q + ' ORDER by ccode'
    fill_slots(Sq, USq, Cq, UCq, no_stream=False)
    #Pq1 = 'SELECT rollno, segment1, segment2, segment3, load FROM pg_ta WHERE rollno \
    #        LIKE \'ee19m%\' OR rollno LIKE \'ee19r%\' or rollno LIKE \'ee18r%\''
    #Pq2 = 'SELECT ccode, req_tas, segments, alloc_ta_list, num_ta FROM allocation WHERE \
    #    (ccode LIKE \'EE%\' or ccode LIKE \'ID%\' or ccode LIKE \'AI%\')'
    #pprint('test', Pq1)
    #pprint('test', Pq2)
    
    print(allocation)
    write_alloc_sheet()


import sys
if __name__=="__main__":
    if len(sys.argv)>1:
        if sys.argv[1] == 'template':
            gen_template()
        if sys.argv[1] == '-f':
            make_allotment(fname=sys.argv[2])
    else:
        make_allotment()

